import java.util.Scanner;

public class App {

    /**
     * Program utama
     * 
     * @param args Unused
     * @throws Exception Unused
     */
    public static void main(String[] args) throws Exception {
        Scanner kScanner = new Scanner(System.in);
        int A = kScanner.nextInt();
        String operator = kScanner.next();
        int B = kScanner.nextInt();
        int hasil = 0;

        switch (operator) {
            case "-":
                hasil = A - B;
                break;
            case "+":
                hasil = A + B;
                break;
            case "*":
                hasil = A * B;
                break;
            case "/":
                hasil = A / B;
                break;
            case "%":
                hasil = A % B;
                break;

        }

        System.out.println(hasil);

        kScanner.close();
    }
}
